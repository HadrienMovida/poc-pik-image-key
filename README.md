# Image Key

Compute the image key from the raw image

## Arguments
- Image path : `-i image_path.extension`
- String : `-k my_string` (Concat. GTIN & Variant)

Example : `get_picture_key -i=MY_IMAGE.jpg -k=GTINVARIANT`  

## Building with Docker

- Linux : `docker run --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp golang:1.17 go build -v -o ./dist/linux/get_picture_key`
- Mac : `docker run --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp -e GOOS=darwin -e GOARCH=amd64 golang:1.17 go build -v -o ./dist/mac/get_picture_key.dmg`
- Windows : `docker run --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp -e GOOS=windows -e GOARCH=amd64 golang:1.17 go build -v -o ./dist/windows/get_picture_key.exe`
