package main

import (
	"crypto/md5"
	"encoding/hex"
	"flag"
	"fmt"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"os"
)

func main() {
	filepath := flag.String("i", "", "image path file")
	metadataString := flag.String("k", "", "metadata string")

	flag.Parse()
	imgFile, err := os.Open(*filepath)
	if err != nil {
		fmt.Printf("file %q not found on disk: %v\n", *filepath, err)
		os.Exit(1)
	}

	img, _, err := image.Decode(imgFile)
	errFile := imgFile.Close()
	if err != nil || errFile != nil{
		os.Exit(1)
	}
	bounds := img.Bounds()

	var imgRGB []byte
	metadataByte := []byte(*metadataString)
	imgRGB = append(imgRGB, metadataByte...)

	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			r, g, b, _ := img.At(x, y).RGBA()

			imgRGB = append(imgRGB, uint8(r>>8))
			imgRGB = append(imgRGB, uint8(g>>8))
			imgRGB = append(imgRGB, uint8(b>>8))
		}
	}

	sum := md5.Sum(imgRGB)
	fmt.Println(hex.EncodeToString(sum[:]))
}
